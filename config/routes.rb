Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :chatrooms, param: :slug
  resources :messages

  root 'home#index'

  get '/signup', to: "signup#new"
  post '/signup', to: "signup#create"

  get 'signin', to: "sessions#new"
  post '/signin', to: "sessions#create"
  delete '/signout', to: "sessions#destroy"

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'

  # ^ will listen to web socket requests on ws://localhost:/3000/cable
end
