App.messages = App.cable.subscriptions.create('MessagesChannel', {
  received: function(data) {
    $("#messages").removeClass('hidden');
    $('#message_content').val('');

    return $('#messages').append("<p> <b>" + data.user + ": </b>" + data.message + "</p>");
  },
});
