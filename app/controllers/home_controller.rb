class HomeController < ApplicationController
  skip_before_action :authenticate_user!

  layout 'no_margin'

  def index
  end
end
